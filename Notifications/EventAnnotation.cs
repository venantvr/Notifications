using System;

namespace Toolbox.Notifications
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class EventAnnotation : Attribute
    {
        public EventAnnotation(string annotation)
        {
            Annotation = annotation;
        }

        public string Annotation { get; }
    }
}